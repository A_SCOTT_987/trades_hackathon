package com.training.hackathon.repository;

import com.training.hackathon.entities.StatusCodeHistory;

import com.training.hackathon.entities.Trades;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface StatusCodeHistoryRepository extends JpaRepository<StatusCodeHistory, Integer>{
    List<StatusCodeHistory> findByTradeId(Integer tradeId);
}