package com.training.hackathon.repository;

import com.training.hackathon.entities.Portfolios;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface PortfoliosRepository extends JpaRepository<Portfolios, Integer> {

}
