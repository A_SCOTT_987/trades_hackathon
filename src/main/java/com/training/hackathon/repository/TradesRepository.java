package com.training.hackathon.repository;

import com.training.hackathon.entities.Trades;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface TradesRepository extends JpaRepository<Trades, Integer> {
    List<Trades> findByStockTicker(String stockTicker);

    List<Trades> findByStatusCode(int statusCode);

    @Query(value = "SELECT (SELECT SUM(VOLUME) FROM TRADES" +
            " WHERE BUYORSELL = 'BUY' AND STOCKTICKER = ?1) - (SELECT SUM(VOLUME) FROM" +
            " TRADES WHERE BUYORSELL = 'SELL' AND STOCKTICKER = ?1)", nativeQuery = true)
    Integer findTotalVolumeStock(String stockTicker);

    @Query(value = "SELECT (SELECT SUM(VOLUME * PRICE) FROM TRADES" +
            " WHERE BUYORSELL = 'BUY' AND STOCKTICKER = ?1) - (SELECT SUM(VOLUME * PRICE) FROM" +
            " TRADES WHERE BUYORSELL = 'SELL' AND STOCKTICKER = ?1)", nativeQuery = true)
    Integer findTotalPriceStock(String stockTicker);

    @Query(value = "SELECT STOCKTICKER," +
            "       SUM(CASE " +
            "           WHEN BUYORSELL='sell' THEN -1*(PRICE*VOLUME)" +
            "           ELSE (PRICE*VOLUME)" +
            "       END) AS TOTAL_HELD_VALUE FROM TRADES WHERE PORTFOLIOID = ?1 GROUP by STOCKTICKER"
            , nativeQuery = true)
    List<String> findTotalValueOfPortfolio(int portfolioId);
}
