package com.training.hackathon.service;

import com.training.hackathon.entities.StatusCodeHistory;
import com.training.hackathon.entities.Trades;
import com.training.hackathon.repository.StatusCodeHistoryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusCodeHistoryService {

    @Autowired
    StatusCodeHistoryRepository statusCodeHistoryRepository;

    public List<StatusCodeHistory> findAll(){
        return statusCodeHistoryRepository.findAll();
    }

    public StatusCodeHistory save(StatusCodeHistory statusCodeHistory){
        return statusCodeHistoryRepository.save(statusCodeHistory);
    }

    public List<StatusCodeHistory> findByTradeId(Integer tradeId) {
        return statusCodeHistoryRepository.findByTradeId(tradeId);
    }
}