package com.training.hackathon.service;

import com.training.hackathon.entities.Portfolios;
import com.training.hackathon.entities.Trades;
import com.training.hackathon.repository.PortfoliosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Column;
import javax.sound.sampled.Port;
import java.util.List;

@Service
public class PortfoliosService {

    @Autowired
    PortfoliosRepository portfoliosRepository;

    public List<Portfolios> findAll(){
        return portfoliosRepository.findAll();
    }

    public Portfolios findById(Integer id) {
        return portfoliosRepository.findById(id).get();
    }

    public Portfolios update(Portfolios portfolios) {
        portfoliosRepository.findById(portfolios.getId()).get();
        portfolios.setNetWorth();
        return portfoliosRepository.save(portfolios);
    }

    public Portfolios updatePartial(Portfolios portfolios, Integer id) {
        Portfolios updatePortfolios;
        updatePortfolios = portfoliosRepository.findById(id).get();

        if (portfolios.getOwner() != "") {
            updatePortfolios.setOwner(portfolios.getOwner());
        }

        if (portfolios.getTotalCash() != 0) {
            updatePortfolios.setTotalCash(portfolios.getTotalCash());
        }

        if (portfolios.getTotalCrypto() != 0) {
            updatePortfolios.setTotalCrypto(portfolios.getTotalCrypto());
        }

        if (portfolios.getTotalStock() != 0) {
            updatePortfolios.setTotalStock(portfolios.getTotalStock());
        }

        updatePortfolios.setNetWorth();

        return portfoliosRepository.save(updatePortfolios);
    }

    public Portfolios save(Portfolios portfolios){ return portfoliosRepository.save(portfolios); }

    public void delete(Integer id) { portfoliosRepository.deleteById(id); }
}
