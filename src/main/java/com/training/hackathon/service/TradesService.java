package com.training.hackathon.service;

import com.training.hackathon.entities.Portfolios;
import com.training.hackathon.entities.StatusCodeHistory;
import com.training.hackathon.entities.Trades;
import com.training.hackathon.repository.TradesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TradesService {

    @Autowired
    TradesRepository tradesRepository;

    @Autowired
    PortfoliosService portfoliosService;

    @Autowired
    StatusCodeHistoryService statusCodeHistoryService;

    public List<Trades> findAll(){
        return tradesRepository.findAll();
    }

    public Trades findById(Integer id) {
        return tradesRepository.findById(id).get();
    }

    public List<Trades> findByStockTicker(String stockTicker){
        return tradesRepository.findByStockTicker(stockTicker);
    }

    public List<Trades> findByStatusCode(int statusCode){
        return tradesRepository.findByStatusCode(statusCode);
    }

    public Integer findTotalVolumeStock(String stockTicker){
        return tradesRepository.findTotalVolumeStock(stockTicker);
    }

    public Integer findTotalPriceStock(String stockTicker){
        return tradesRepository.findTotalPriceStock(stockTicker);
    }

    public Trades save(Trades trades){
        Portfolios portfolio = portfoliosService.findById(trades.getPortfolioID());

        double tradeValue = trades.getVolume() * trades.getPrice();

        if(trades.getBuyOrSell().equalsIgnoreCase("buy")){
            if(trades.getTradeType().equalsIgnoreCase("stock")){
                portfolio.setTotalStock(portfolio.getTotalStock() + tradeValue);
            }
            else if(trades.getTradeType().equalsIgnoreCase("crypto")){
                portfolio.setTotalCrypto(portfolio.getTotalCrypto() + tradeValue);
            }
            else if(trades.getTradeType().equalsIgnoreCase("currency")){
                portfolio.setTotalCash(portfolio.getTotalCash() + tradeValue);
            }
        }
        else{
            if(trades.getTradeType().equalsIgnoreCase("stock")){
                portfolio.setTotalStock(portfolio.getTotalStock() - tradeValue);
            }
            else if(trades.getTradeType().equalsIgnoreCase("crypto")){
                portfolio.setTotalCrypto(portfolio.getTotalCrypto() - tradeValue);
            }
            else if(trades.getTradeType().equalsIgnoreCase("currency")){
                portfolio.setTotalCash(portfolio.getTotalCash() - tradeValue);
            }
        }
        portfoliosService.update(portfolio);

        return tradesRepository.save(trades);
    }

    public Trades update(Trades trades) {
        Trades updateTrade = tradesRepository.findById(trades.getId()).get();

        if (trades.getStatusCode() != null &&
                !trades.getStatusCode().equals(updateTrade.getStatusCode())) {

            StatusCodeHistory statusCodeHistory = new StatusCodeHistory();
            statusCodeHistory.setTradeId(trades.getId());
            statusCodeHistory.setOriginalStatusCode(updateTrade.getStatusCode());
            statusCodeHistory.setNewStatusCode(trades.getStatusCode());
            statusCodeHistory.setCreatedTimestamp(new Date(System.currentTimeMillis()));
            statusCodeHistoryService.save(statusCodeHistory);
        }

        return tradesRepository.save(trades);
    }

    public Trades updatePartial(Trades trades, Integer id) {
        Trades updateTrade;
        updateTrade = tradesRepository.findById(id).get();

        if (trades.getStockTicker() != null) {
            updateTrade.setStockTicker(trades.getStockTicker());
        }

        if (trades.getPrice() != 0) {
            updateTrade.setPrice(trades.getPrice());
        }

        if (trades.getVolume() != null) {
            updateTrade.setVolume(trades.getVolume());
        }

        if (trades.getBuyOrSell() != null) {
            updateTrade.setBuyOrSell(trades.getBuyOrSell());
        }

        if (trades.getStatusCode() != null &&
                !trades.getStatusCode().equals(updateTrade.getStatusCode())) {

            StatusCodeHistory statusCodeHistory = new StatusCodeHistory();
            statusCodeHistory.setTradeId(id);
            statusCodeHistory.setOriginalStatusCode(updateTrade.getStatusCode());
            statusCodeHistory.setNewStatusCode(trades.getStatusCode());
            statusCodeHistory.setCreatedTimestamp(new Date(System.currentTimeMillis()));
            statusCodeHistoryService.save(statusCodeHistory);

            updateTrade.setStatusCode(trades.getStatusCode());
        }

        return tradesRepository.save(updateTrade);
    }

    public void delete(Integer id) {
        tradesRepository.deleteById(id);
    }

    public List<String> findTotalValueOfPortfolio(int portfolioId){
        return tradesRepository.findTotalValueOfPortfolio(portfolioId);
    }
}
