package com.training.hackathon.entities;

import javax.persistence.*;

@Entity(name="Portfolios")
public class Portfolios {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="owner")
    private String owner;

    @Column(name="netWorth")
    private double netWorth;

    @Column(name="totalCash")
    private double totalCash;

    @Column(name="totalCrypto")
    private double totalCrypto;

    @Column(name="totalStock")
    private double totalStock;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public double getNetWorth() {
        return netWorth;
    }

    public void setNetWorth() {
        this.netWorth = totalCash + totalCrypto + totalStock;
    }

    public double getTotalCash() {
        return totalCash;
    }

    public void setTotalCash(double totalCash) {
        this.totalCash = totalCash;
    }

    public double getTotalCrypto() {
        return totalCrypto;
    }

    public void setTotalCrypto(double totalCrypto) {
        this.totalCrypto = totalCrypto;
    }

    public double getTotalStock() {
        return totalStock;
    }

    public void setTotalStock(double totalStock) {
        this.totalStock = totalStock;
    }

    @Override
    public String toString() {
        return "Portfolios{" +
                "id=" + id +
                ", owner='" + owner + '\'' +
                ", netWorth=" + netWorth +
                ", totalCash=" + totalCash +
                ", totalCrypto=" + totalCrypto +
                ", totalStock=" + totalStock +
                '}';
    }
}
