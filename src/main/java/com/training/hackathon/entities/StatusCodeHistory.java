package com.training.hackathon.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity(name="StatusCodeHistory")
public class StatusCodeHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="tradeId")
    private Integer tradeId;

    @Column(name="originalStatusCode")
    private Integer originalStatusCode;

    @Column(name="newStatusCode")
    private Integer newStatusCode;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTimestamp = new Date(System.currentTimeMillis());

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTradeId() {
        return tradeId;
    }

    public void setTradeId(Integer tradeId) {
        this.tradeId = tradeId;
    }

    public Integer getOriginalStatusCode() {
        return originalStatusCode;
    }

    public void setOriginalStatusCode(Integer originalStatusCode) {
        this.originalStatusCode = originalStatusCode;
    }

    public Integer getNewStatusCode() {
        return newStatusCode;
    }

    public void setNewStatusCode(Integer newStatusCode) {
        this.newStatusCode = newStatusCode;
    }

    public Date getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(Date createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }


}