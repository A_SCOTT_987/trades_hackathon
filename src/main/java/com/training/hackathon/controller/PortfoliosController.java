package com.training.hackathon.controller;

import com.training.hackathon.entities.Portfolios;
import com.training.hackathon.service.PortfoliosService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/portfolios")
public class PortfoliosController {

    @Autowired
    private PortfoliosService portfoliosService;

    private static final Logger LOG = LoggerFactory.getLogger(PortfoliosController.class);

    @GetMapping("/findAll")
    public List<Portfolios> findAll() {
        LOG.debug("find all records in portfolios");
        return portfoliosService.findAll();
    }

    @GetMapping("/findId/{id}")
    public ResponseEntity<Portfolios> findById(@PathVariable Integer id) {
        try {
            LOG.debug("find successful for id: [" + id + "]");
            return new ResponseEntity<Portfolios>(portfoliosService.findById(id), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("find unsuccessful for id: [" + id + "]");
            return new ResponseEntity<Portfolios>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Portfolios> create(@RequestBody Portfolios portfolios){
        LOG.debug("create successful for portfolio: [" + portfolios + "]");
        return new ResponseEntity<Portfolios>(portfoliosService.save(portfolios), HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Portfolios> update(@RequestBody Portfolios portfolios) {
        try {
            LOG.debug("update successful for trade: [" + portfolios + "]");
            return new ResponseEntity<Portfolios>(portfoliosService.update(portfolios), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("update failed for unknown trade: [" + portfolios + "]");
            return new ResponseEntity<Portfolios>(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/update/{id}")
    public ResponseEntity<Portfolios> updatePartial(@RequestBody Portfolios portfolios, @PathVariable Integer id) {
        try {
            LOG.debug("Patch successful for portfolio: [" + portfolios + "]");
            return new ResponseEntity<Portfolios>(portfoliosService.updatePartial(portfolios, id), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("update failed for unknown portfolio: [" + portfolios + "]");
            return new ResponseEntity<Portfolios>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Portfolios> delete(@PathVariable Integer id) {
        try {
            LOG.debug("delete successful for id: [" + id + "]");
            portfoliosService.delete(id);
        } catch(EmptyResultDataAccessException ex) {
            LOG.debug("delete failed for unknown id: [" + id + "]");
            return new ResponseEntity<Portfolios>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Portfolios>(HttpStatus.NO_CONTENT);
    }
}
