package com.training.hackathon.controller;

import com.training.hackathon.entities.Portfolios;
import com.training.hackathon.service.StatusCodeHistoryService;

import java.util.List;
import java.util.NoSuchElementException;

import com.training.hackathon.entities.StatusCodeHistory;
import com.training.hackathon.entities.Trades;
import com.training.hackathon.service.TradesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import io.swagger.v3.oas.annotations.parameters.RequestBody;

@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/statusCodeHistory")
public class StatusCodeHistoryController {

    @Autowired
    private StatusCodeHistoryService statusCodeHistoryService;

    private static final Logger LOG = LoggerFactory.getLogger(TradesController.class);

    @GetMapping("/findAll")
    public List<StatusCodeHistory> findAll() {
        return statusCodeHistoryService.findAll();
    }

    @PostMapping("/create")
    public ResponseEntity<StatusCodeHistory> create(@RequestBody StatusCodeHistory statusCodeHistory) {
        System.out.println(statusCodeHistory.getOriginalStatusCode());
        return new ResponseEntity<StatusCodeHistory>(statusCodeHistoryService.save(statusCodeHistory), HttpStatus.CREATED);
    }

    @GetMapping("/findByTradeId/{tradeId}")
    public ResponseEntity<List<StatusCodeHistory>> findByTradeId(@PathVariable Integer tradeId) {
        try {
            LOG.debug("find successful for trade id: [" + tradeId + "]");
            return new ResponseEntity<List<StatusCodeHistory>>(statusCodeHistoryService.findByTradeId(tradeId), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("find unsuccessful for trade id: [" + tradeId + "]");
            return new ResponseEntity<List<StatusCodeHistory>>(HttpStatus.NOT_FOUND);
        }
    }
}