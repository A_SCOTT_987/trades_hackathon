package com.training.hackathon.controller;

import com.training.hackathon.entities.Trades;
import com.training.hackathon.service.TradesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/trades")
public class TradesController {

    @Autowired
    private TradesService tradesService;

    private static final Logger LOG = LoggerFactory.getLogger(TradesController.class);

    @GetMapping("/findAll")
    public List<Trades> findAll() {
        LOG.debug("find all records in trades");
        return tradesService.findAll();
    }

    @GetMapping("/findId/{id}")
    public ResponseEntity<Trades> findById(@PathVariable Integer id) {
        try {
            LOG.debug("find successful for id: [" + id + "]");
            return new ResponseEntity<Trades>(tradesService.findById(id), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("find unsuccessful for id: [" + id + "]");
            return new ResponseEntity<Trades>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/findTicker/{stockTicker}")
    public ResponseEntity<List<Trades>> findByStockTicker(@PathVariable String stockTicker) {
        try {
            LOG.debug("find successful for stock ticker: [" + stockTicker + "]");
            return new ResponseEntity<List<Trades>>(tradesService.findByStockTicker(stockTicker), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("find unsuccessful for stock ticker: [" + stockTicker + "]");
            return new ResponseEntity<List<Trades>>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/findStatus/{statusCode}")
    public ResponseEntity<List<Trades>> findByStatusCode(@PathVariable int statusCode) {
        try {
            LOG.debug("find successful for status code: [" + statusCode + "]");
            return new ResponseEntity<List<Trades>>(tradesService.findByStatusCode(statusCode), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("find unsuccessful for status code: [" + statusCode + "]");
            return new ResponseEntity<List<Trades>>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/findTotalVolumeStock/{stockTicker}")
    public ResponseEntity<Integer> findTotalVolumeStock(@PathVariable String stockTicker) {
        try {
            LOG.debug("find successful for stock ticker : [" + stockTicker + "]");
            return new ResponseEntity<Integer>(tradesService.findTotalVolumeStock(stockTicker), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("find unsuccessful for stock ticker: [" + stockTicker + "]");
            return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/findTotalPriceStock/{stockTicker}")
    public ResponseEntity<Integer> findTotalPriceStock(@PathVariable String stockTicker) {
        try {
            LOG.debug("find successful for stock ticker: [" + stockTicker + "]");
            return new ResponseEntity<Integer>(tradesService.findTotalPriceStock(stockTicker), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("find unsuccessful for stock ticker: [" + stockTicker + "]");
            return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/findTotalValueOfPortfolio/{portfolioId}")
    public ResponseEntity<List<String>> findTotalValueOfPortfolio(@PathVariable int portfolioId){
        try {
            LOG.debug("find successful for portfolio id: [" + portfolioId + "]");
            return new ResponseEntity<List<String>>(tradesService.findTotalValueOfPortfolio(portfolioId), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("find unsuccessful for portfolio id: [" + portfolioId + "]");
            return new ResponseEntity<List<String>>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Trades> create(@RequestBody Trades trades){
        LOG.debug("create successful for trade: [" + trades + "]");
        return new ResponseEntity<Trades>(tradesService.save(trades), HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Trades> update(@RequestBody Trades trades) {
        try {
            LOG.debug("update successful for trade: [" + trades + "]");
            return new ResponseEntity<Trades>(tradesService.update(trades), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("update failed for unknown trade: [" + trades + "]");
            return new ResponseEntity<Trades>(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/update/{id}")
    public ResponseEntity<Trades> patch(@RequestBody Trades trades, @PathVariable Integer id) {
        try {
            LOG.debug("Patch successful for trade: [" + trades + "]");
            return new ResponseEntity<Trades>(tradesService.updatePartial(trades, id), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("update failed for unknown trade: [" + trades + "]");
            return new ResponseEntity<Trades>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Trades> delete(@PathVariable Integer id) {
        try {
            LOG.debug("delete successful for id: [" + id + "]");
            tradesService.delete(id);
        } catch(EmptyResultDataAccessException ex) {
            LOG.debug("delete failed for unknown id: [" + id + "]");
            return new ResponseEntity<Trades>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Trades>(HttpStatus.NO_CONTENT);
    }
}
