package com.training.hackathon.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.training.hackathon.entities.Trades;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class TradesControllerTest {

    private static final String baseRestUrl = "/api/v1/trades";

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void tradesControllerFindAllSuccess() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get(baseRestUrl + "/findAll"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        List<Trades> trades = new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<List<Trades>>() { });

        assertThat(trades.size()).isGreaterThan(0);
    }

    @Test
    public void tradesControllerFindAllFail() throws Exception{
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/trade"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void tradesControllerFindByID() throws Exception{
        int testId = 1;

        MvcResult mvcResult = this.mockMvc.perform(get(baseRestUrl + "/findId" + "/" + testId))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        Trades trades = new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<Trades>() { });

        assertThat(trades.getId()).isEqualTo(testId);
    }

    @Test
    public void tradesControllerFindByIDFailed() throws Exception{
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/trades/findId/1000"))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    public void tradesControllerFindByStockTicker() throws Exception{
        String testStockTicker="AAPL";
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/trades/findTicker" + "/" + testStockTicker))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void tradesControllerFindByStockTickerFail() throws Exception{
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/trades/findTicker/"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void tradesControllerFindByStatusCode() throws Exception{
        Integer testStatusCode=2;
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/trades/findStatus" + "/" + testStatusCode))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void tradesControllerFindByStatusCodeFail() throws Exception{
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/trades/findStatus/"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void tradesControllerFindByVolume() throws Exception{
        String stockTicker="AAPL";
        int testVolume = 2;
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/trades/findTotalVolumeStock" + "/" + stockTicker))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void tradesControllerFindByVolumeFail() throws Exception{
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/trades/findStatus/"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void testDeleteTrade() throws Exception {
        // 1. setup stuff
        int testId = 2;

        // 2. call method under test
        MvcResult mvcResult = this.mockMvc.perform(delete(baseRestUrl + "/delete/" + testId)
                .header("Content-Type", "application/json"))
                .andDo(print()).andExpect(status().isNoContent()).andReturn();

        // 3. verify the results
        // ensure this id is now Not Found
        this.mockMvc.perform(get(baseRestUrl + "/delete/" + testId))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

//    @Test
//    public void testCreateTrade() throws Exception {
//        // 1. setup stuff
//        int testId = 3;
//        String testStockTicker = "AAPL";
//        Double testPrice = 100.0;
//        Integer testVolume = 3;
//        String testBuyOrSell = "BUY";
//        Integer testStatusCode = 2;
//
//
//        Trades testTrades = new Trades();
//        testTrades.setId(testId);
//        testTrades.setBuyOrSell(testBuyOrSell);
//        testTrades.setPrice(testPrice);
//        testTrades.setStatusCode(testStatusCode);
//        testTrades.setStockTicker(testStockTicker);
//        testTrades.setVolume(testVolume);
//
//
//
//        // convert the testShipper Java object into a JSON string
//        ObjectMapper mapper = new ObjectMapper();
//        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
//        String requestJson=ow.writeValueAsString(testTrades);
//
//        System.out.println(requestJson);
//
//        // 2. call method under test send a HTTP POST with the test Shipper in JSON format
//        MvcResult mvcResult = this.mockMvc.perform(post(baseRestUrl + "/create")
//                .header("Content-Type", "application/json")
//                .content(requestJson))
//                .andDo(print()).andExpect(status().isCreated()).andReturn();
//
//        // 3. verify the results - first convert the returned JSON into a Shipper object
//        Trades trades = new ObjectMapper().readValue(
//                mvcResult.getResponse().getContentAsString(),
//                new TypeReference<Trades>() { });
//
//        // check that the fields in the returned object are correct
//        assertThat(trades.getId()).isEqualTo(testId);
//        assertThat(trades.getStockTicker()).isEqualTo(testStockTicker);
//        assertThat(trades.getBuyOrSell()).isEqualTo(testBuyOrSell);
//        assertThat(trades.getPrice()).isEqualTo(testPrice);
//        assertThat(trades.getVolume()).isEqualTo(testVolume);
//    }

    @Test
    public void testEditTrade() throws Exception {
        // 1. setup stuff
        Integer testId = 1;
        String testStockTicker = "AAPL";
        Double testPrice = 100.0;
        Integer testVolume = 3;
        String testBuyOrSell = "BUY";
        Integer testStatusCode = 2;

        Trades testTrades = new Trades();
        testTrades.setId(testId);
        testTrades.setBuyOrSell(testBuyOrSell);
        testTrades.setPrice(testPrice);
        testTrades.setStatusCode(testStatusCode);
        testTrades.setStockTicker(testStockTicker);
        testTrades.setVolume(testVolume);

        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(testTrades);

        System.out.println(requestJson);

        // 2. call method under test
        MvcResult mvcResult = this.mockMvc.perform(put(baseRestUrl + "/update")
                .header("Content-Type", "application/json")
                .content(requestJson))
                .andDo(print()).andExpect(status().isOk()).andReturn();

        // 3. verify the results
        Trades trades = new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<Trades>() { });

        assertThat(trades.getStockTicker()).isEqualTo(testStockTicker);
        assertThat(trades.getBuyOrSell()).isEqualTo(testBuyOrSell);
        assertThat(trades.getPrice()).isEqualTo(testPrice);
        assertThat(trades.getVolume()).isEqualTo(testVolume);

        // verify the edit took place
        mvcResult = this.mockMvc.perform(get(baseRestUrl + "/findId/" + testId))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();


        trades = new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<Trades>() { });

        assertThat(trades.getId()).isEqualTo(testId);
        assertThat(trades.getStockTicker()).isEqualTo(testStockTicker);
        assertThat(trades.getBuyOrSell()).isEqualTo(testBuyOrSell);
        assertThat(trades.getPrice()).isEqualTo(testPrice);
        assertThat(trades.getVolume()).isEqualTo(testVolume);
    }
}
