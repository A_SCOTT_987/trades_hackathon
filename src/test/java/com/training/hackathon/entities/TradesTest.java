package com.training.hackathon.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TradesTest {

    private Trades trades;

    private static final double testPrice = 100;
    private static final String testBuy = "BUY";
    @BeforeEach
    public void setup(){
        this.trades = new Trades();
    }

    @Test
    public void setPriceTest(){
        this.trades.setPrice(100);
        assertEquals(this.trades.getPrice(), 100);
    }

    @Test
    public void setBuyPrice(){
        this.trades.setBuyOrSell("BUY");
        assertEquals(this.trades.getBuyOrSell(), "BUY");
    }

    @Test
    public void setStatusCodeTest(){
        this.trades.setStatusCode(1);
        assertEquals(this.trades.getStatusCode(), 1);
    }
}
