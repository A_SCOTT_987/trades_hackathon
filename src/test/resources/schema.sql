CREATE TABLE IF NOT EXISTS Trades
    (id INT NULL AUTO_INCREMENT,
    stockTicker VARCHAR(255) NULL,
    price DOUBLE null,
    volume INT null,
    buyOrSell VARCHAR(255),
    statusCode INT null,
    PRIMARY KEY(ID));

INSERT INTO Trades (id, StockTicker, price, volume, buyOrSell, statusCode) VALUES
('1', 'AAPL', '100', '3', 'BUY', '2'),
('2', 'AMZN', '150', '2', 'SELL', '3');